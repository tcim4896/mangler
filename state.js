/* %%%%%%%%%%%%%%%%%%%%%%%%%%% state.js */
mState = {
	elements:[],
	services:{
		menu:{
			open:{
				id:0,
				open:false
			},
		},
		router:{
			currentPage: 0,
			routes:{}
		},
		langCheckup:{
			inputStr:""
		},
		cursor: {
			x:0,
			y:0,
		},
		replace:{
			term:"",
			replacement:""
		},
		exclude:{
			char:""
		},
		extra:{
			char:""
		},
		encrypt:{
			type:"abcup"
		},
		encode:{
			type:""
		},
		applied:[]
	}
};
_=mState.services;
menuStructure = [
 	{
	    text: "Dictea",
	    event: {type:"open",path:0},
	    list: [
	      {
	        text: "Mangler",
	        event: {type:"route",path:"mangler"
	        }
	      },
	      {
	        text: "Langcheckup",
	        event: {type:"route",path:"langcheckup"}
	      },
	      {
	        text: "Dictionary",
	        event: {type:"route",path:"dictionary"
	        }
	      },
	      {
	        text: "Recognite",
	        event: {type:"route",path:"dictionary"
	        }
	      },
	      {
	        text: "Recognite",
	        event: {type:"route",path:"dictionary"
	        }
	      },
	      	      {
	        text: "Recognite",
	        event: {type:"route",path:"dictionary"
	        }
	      },
	      	      {
	        text: "Recognite",
	        event: {type:"route",path:"dictionary"
	        }
	      },		      
	    ],
	},
	{
	    text: "Leke.js",
	    event: {type:"open",path:1},
	    list: [
	       {
	        text: "Quick start",
	        event: {type:"route",path:"langcheckup"}
	      },   
	      {
	        text: "API Reference",
	        event: {type:"route",path:"mangler"
	        }
	      },
	      {
	        text: "Downloads",
	        event: {type:"route",path:"leke/downloads"
	        }
	      },
	    ]
  	},
	{
	    text: "Noteshare",
	    event: {type:"open",path:2},
	    list:[
		    {
		        text: "Quick start",
		        event: {type:"route",path:"noteshare"}
		    },
			{
		        text: "Register",
		        event: {type:"ref",path:"http://pinterest.com/"}
		    },
	    ]
	},
];