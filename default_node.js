/*%%%%%%%%%%%%%%%%%%%%%% default_node.js*/
var defaultNode = {
	identifier: 'node',
	tag: 'div',
	text: 'defaultNode',
	style: {
		position: "relative",
		backgroundColor: "rgba(0,0,0,0.2)",
		width: "100%",
		lineHeight: "40px",
		paddingLeft: "10px",
		color: "#fff",
		boxSizing: "border-box"
	},
	attributes:{
		"contentEditable": true,
		"class": "node",
	},
	eventListeners:[{
		type: 'mousedown', 
		fn: function(elm){
			return function(e){
				cl("down", e);
				mStateMap[identifier].keyDown = true;
				mStateMap[identifier].e = e;
			};
		},
	}],
	children:[{a:"child"}],
}