/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%% dom-utils.js*/
root=document.querySelector("[root]");

function o(args) {
	let r = document.createElement("div");
	for(let prop in args){
		switch(prop){
			case "text":
				r.textContent = args[prop];
			break;
			case "class":
				r.setAttribute("class", args[prop]);
			break;
			case "siblings":
				for (let sibling of args[prop]) {
    			r.appendChild(sibling);
  			}
  			break;
  			case "id":
  				mState[args[prop]] = r;
  			break;
  			default:
  				r.setAttribute("class", "input")
  			break;
		}
	}
  return r;
}

/*
	attach event listener to element
	elm > type > fn
*/
function e(elm,type,fn) {
	function ffkeys(event){ // key validator
		let r;
		switch(event.key){
			case "Shift":
				r="";
			break;
			case "Tab":
				r="";
			break;
			case "Backspace":
				r="";
			break;
			// Ctrl, Alt -> dumpfile[valid]
			default:
				r=event.key;
			break;
		}
		return r;
	}

	if(true){ // if elm.ffkeys
		elm.addEventListener(type,function(e){
			e.value=ffkeys(e);
			fn.call(e)
		});
	}else{
		elm.addEventListener(type,fn)
	}
  return elm;
}
/*^*/
function ee(...args){
  let elm,events;
  for(let arg of args){
    switch(typeof arg){
      case "object":
        elm=arg;
      break;
      case "string":
        events=arg.split(" ");
      break;
      case "function":
        cl(events)
        for(let event of events){
          elm.addEventListener(event,function(e){
            arg.call(e);
          })
        }
      break;
      default:
        console.log("Syntax error..")
      break;
    }
  }
  return elm;
}

function json(value){
	return JSON.stringify(value);
}

function s(id) {
  return mState[id];
}

function b(elm, sibling) {
  elm.appendChild(sibling);
}

function input(placeholder){
	let elm=document.createElement("input");
	elm.setAttribute("class","input")
	return elm;
}

function btn(text){
	let div=document.createElement("div");
	div.innerText=text
	div.setAttribute("class","btn")
	return div;
}

function text(text){
	let div=document.createElement("div");
	div.innerText=text
	div.setAttribute("class","btn")
	return div;
}

function dropdown(props){
	/* tdoo at cursor fixed position */
	let dropdown=document.createElement("div");
	dropdown.open=false;
  	dropdown.textContent=props.text;
	dropdown.setAttribute("class","dropdown");
  	dropdown.addEventListener("click",function(e){
	    s(props.id).open=!s(props.id).open;
	    if(!s(props.id).open){
	    	s(props.id).style.height="20px";
	    }else{
	    	s(props.id).style.height=(props.options.length+1)*20+"px";
	    }
 	})
 	let icon=document.createElement("div");
	icon.setAttribute("class","icon")
	icon.innerHTML="&#x25BC";
 	dropdown.appendChild(icon);

	for(let option of props.options){
		let opt=document.createElement("div");
		opt.setAttribute("class","option")
		opt.textContent=option.text;
		opt.addEventListener("click",function(e){
			cl(option.value)
			_.encrypt.type=option.value;
		})
    	dropdown.
    	appendChild(opt);
	}
  	mState[props.id]=dropdown;
	return dropdown;
}