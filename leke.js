// propchaining
d={
  value:{},
  fn(prop){
    this.value[prop]=prop;
    return this;
  }
};

d.fn(0).fn(1)

ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

function stateChange(service){
	function clear(obj){
		for(let prop in obj){
			if(typeof prop=="string"){
				obj[prop]="";
			}
		}
		return;
	}
	clear(service) // prope to get dom modified seriously clean

	s("applied").innerHTML="";
	if(_.applied.length>0){
		_.applied.forEach(function(method){
			switch(method.event){
				case "replace":
					b(s("applied"),o({id:method.id,class:"method",siblings:[
						e(btn("x"),"click",e=>{
							_.applied=_.applied.filter(function(m){
								return method.id !== m.id;
							})
							stateChange()
						}),
						text(method.event),
						text(method.term),
						text(method.replacement)
					]}))
					s("mangled-text").textContent=window[method.event](
						s("mangled-text")
						.textContent,
						method.term,
						method.replacement
					);
				break;
				case "exclude":
					b(s("applied"),o({id:method.id,class:"method",siblings:[
						e(btn("x"),"click",e=>{
							_.applied=_.applied.filter(function(m){
								return method.id !== m.id;
							})
							stateChange()
						}),
						text(method.char),
					]}))
					s("mangled-text")
					.textContent=window[method.event](
						s("mangled-text").textContent,
						method.char,
					);
				break;
				case "toLowerCase":
					b(s("applied"),o({id:method.id,class:"method",siblings:[
						e(btn("x"),"click",e=>{
							_.applied=_.applied.filter(function(m){
								return method.id !== m.id;
							})
							stateChange()
						}),
						text("lowercase")
					]}))
					s("mangled-text")
					.textContent=window[method.event](
						s("mangled-text").textContent); //
				break;
				case "toUpperCase":
					b(s("applied"),o({id:method.id,class:"method",siblings:[
						e(btn("x"),"click",e=>{
							_.applied=_.applied.filter(function(m){
								return method.id !== m.id;
							})
							stateChange()
						}),
						text("uppercase")
					]}))
					s("mangled-text")
					.textContent=window[method.event](
						s("mangled-text").textContent); //
				break;
				case "extra":
					b(s("applied"),o({id:method.id,class:"method",siblings:[
						e(btn("x"),"click",e=>{
							_.applied=_.applied.filter(function(m){
								return method.id !== m.id;
							})
							stateChange()
						}),
						text(method.char),
					]}))

					s("mangled-text")
					.textContent=window[method.event](
						s("mangled-text").textContent,
						method.char,
					);
				break;
				case "encrypt":
					b(s("applied"),o({id:method.id,class:"method",siblings:[
						e(btn("x"),"click",e=>{
							_.applied=_.applied.filter(function(m){
								return method.id !== m.id;
							})
							stateChange()
						}),
						text("encrypt:"+method.type),
					]}))
					s("mangled-text")
					.textContent=window[method.event](s("mangled-text")
						.textContent,_.encrypt.type);
				break;
				case "encode":
					b(s("applied"),o({id:method.id,class:"method",siblings:[
						e(btn("x"),"click",e=>{
							_.applied=_.applied.filter(function(m){
								return method.id !== m.id;
							})
							stateChange()
						}),
						text("encode"),
						text(_.encode.type),
					]}))
					s("mangled-text")
					.textContent=window[method.event](s("mangled-text")
						.textContent,_.encode.type);
				break;														
			}
		})		
	}else{
		s("mangled-text").textContent=s("original-text").textContent;
	}
}

e(document.body,"mousedown",function(e){
	mState['mousedown']=true
})

e(document.body,"mouseup",function(e){
	mState['mousedown']=false
})

e(document.documentElement,"mousemove",function(){
	_.x=this.clientX;
	_.y=this.clientY;
})
// body
o({id:"tools",class:"tools"})
o({id:"applied",class:"applied"})
o({id:"toolbar",class:"toolbar",siblings:[s("tools"),s("applied")]})
e(o({id:"original-text",class:"container",text:ipsum}),"keydown",function(){
	cl(s("mangled-text").textContent+=this.value) // fix bug here
	stateChange();
})
s("original-text").setAttribute("contentEditable", true)
o({id:"mangled-text",class:"container"})
o({id:"viewer",class:"viewer",siblings:[s("original-text"),s("mangled-text")]})
o({id:"mangler",class:"wrapper",siblings:[s("toolbar"),s("viewer")]})
// or propchaining

o({id:"replace",class:"method",siblings:[
	e(input("term"),"keydown",function(){
		_.replace["term"]+=this.value;
	}),
	e(input("replacement"),"keydown",function(){
		_.replace["replacement"]+=this.value;
	}),
	e(btn("replace"),"click", function (e){
		_.applied.push({
			id:_.applied.length,
			event:"replace",
			term:_.replace.term,
			replacement:_.replace.replacement
		})
		stateChange()
	})
]})

o({id:"exclude",class:"method",siblings:[
	e(input("term"),"keydown",function(){
		_.exclude.char+=this.value;
	}),
	e(btn("exclude"),"click",function(){
		_.applied.push({
			id:_.applied.length,
			event:"exclude",
			char:_.exclude.char
		})
		stateChange(_.exclude) // *clear input field
		// clear service variable here; try moving it over to stateChange
	})
]})


o({id:"extra",class:"method",siblings:[
	e(input("term"),"keydown",function(){
		_.extra["char"]+=this.value;
	}),
	e(btn("extra"),"click",function(){
		_.applied.push({
			id:_.applied.length,
			event:"extra",
			char:_.extra.char
		})
		stateChange()
	})
]})


o({id:"lowercase",class:"method",siblings:[
	e(btn("lowercase"),"click",function(){
		_.applied.push({
			id:_.applied.length,
			event:"toLowerCase"
		})
		stateChange()
	})
]})

o({id:"uppercase",class:"method",siblings:[
	e(btn("uppercase"),"click",function(){
		_.applied.push({
			id:_.applied.length,
			event:"toUpperCase"
		})
		stateChange()
	})
]})

o({id:"encrypt",class:"method",siblings:[
	dropdown({id:"encryptionType",text:"type",options:[
		{text:"base64",value:"base64"},
		{text:"abcup",value:"abcup"},
	]}),
	e(btn("encrypt"),"click",function(){
		_.applied.push({
			id:_.applied.length,
			event:"encrypt",
			type:_.encrypt.type,
		})
		stateChange()
	})
]})

o({id:"encode",class:"method",siblings:[
	e(input("term"),"keydown",function(){
		_.encode.type+=this.value;
	}),
	e(btn("encode"),"click",function(){
		_.applied.push({
			id:_.applied.length,
			event:"encode",
			type:_.encode.type,
		})
		stateChange()
	})
]});

// creating ranking service
_.ranking={
	items:{
		382494:{
			points:0,
			accounts:[],
		},
		234890:{
			points:1,
			accounts:[0]
		}
	}
};

function rank(userId,itemId){
	let item=_.ranking.items[itemId];
	if(typeof item !=="undefined"){
		item.points+=item.accounts.indexOf(userId)==-1?
		1:0;
	}
	return item;
}

// cl(rank(0,382494))
// cl(rank(0,234890))

b(s("tools"),s("replace"))
b(s("tools"),s("exclude"))
b(s("tools"),s("extra"))
b(s("tools"),s("lowercase"))
b(s("tools"),s("uppercase"))
b(s("tools"),s("encrypt"))
b(s("tools"),s("encode"))

o({id:"langcheckup", class:"langcheckup",siblings:[
	text("Language Checkup Tool"),
	e(input(),"keydown",function(){
		_.langCheckup.inputStr+=this.value;
	}),
	e(btn("Input"),"click",function(){
		// storing a sentence into localStorage
		store({"key":localStorage.length,"value":_.langCheckup.inputStr})
		_.langCheckup.inputStr="";
	}),
	e(btn("Check"),"click",function(){
		let r=false;
		for(let i=0;i<localStorage.length-1;i++){
			r=localStorage.getItem(i)==_.langCheckup.inputStr;
		}
		if(r){
			b(s("langcheckup"),text("Yes!"))
		}else{
			// check occurance of string in localStorage
			b(s("langcheckup"),text("Sorry no occurance of this language.."))
		}
		
	})
]})

o({id:"noteshare", class:"noteshare",siblings:[
	text("Noteshare"),
	e(btn("Upload"),"click",function(){
		let url ="https://i.pinimg.com/originals/ef/0b/66/ef0b668ba1c367ec629cd9419016ee44.jpg";
		let img=document.createElement("img");
		img.src=url;
		img.style.height="100%";
		img.style.width="auto";
		img.style.border="1px solid #000";
		b(s("noteshare"),img)
	})
]})

o({id:"dictionary", class:"dictionary",siblings:[
	text(`mangle1[ mang-guh l ]
		verb (used with object), man·gled, man·gling.
		to injure severely, disfigure, or mutilate by cutting, slashing, or crushing:
		The coat sleeve was mangled in the gears of the machine.
		to spoil; ruin; mar badly:
		to mangle a text by careless typesetting.`),
]})

o({id:"leke/downloads", class:"dictionary",siblings:[
	text("Leke.js Framework"),
	text("Mangler"),
]})
// register service
function registerService(service){
	_[service.name]=service;
}

o({id:"frontpanel",class:"frontpanel",siblings:[
	text("Welcome!")
]})

registerService({
	name: "router",
	init: function Router(pageId){ // handler for request and routes
		/*
			dynamic switcher(in case of registerRoute) so no switch statement!
		*/
		switch(pageId){
			case "frontpanel":
				b(root,s("frontpanel"))
			break;
			case "langcheckup":
				b(root,s("langcheckup"))
			break;
			case "noteshare":
				b(root,s("noteshare"))
			break;
			case "dictionary":
				b(root,s("dictionary"))
			break;
			case "leke/downloads":
				b(root,s("leke/downloads"))
			break;
			case "mangler":
				b(root,s("mangler"))
			break;
			default:
				b(root,s("frontpanel"))
			break;
		}
	},
	registerRoute: function registerRoute(id,component){
		_.router.routes[id]=component;
	}
})


drawMenu(menuStructure);

_.router.init()
stateChange() //init