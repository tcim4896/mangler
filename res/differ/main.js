
cl=console.log;
function cloneNode(node){
  let r={};
  for(let prop in node){
    if(Array.isArray(node[prop])){//siblings
      r[prop]=[];
      node[prop].forEach(sibling=>r[prop]=cloneNode(sibling))
    }else if(typeof node[prop]=="object"){//styles
      r[prop]={};
      for(let style in node[prop]){
        r[prop][style]=node[prop][style];
      }
    }else{
      r[prop]=node[prop];//normal properties
    }
  }
  return r;
}

function differ(prev,curr){
  let diffs=[];
  function size(obj){
    let num=0;
    for(let prop in obj){
      num+=1;
    }
    return num;
  }
  function diff(prev,curr){
    for(let prop in curr){
      if(typeof prev[prop]=="undefined"){
        diffs.push({prop,state:"undefined"})
      }else if(prev[prop]!==curr[prop]){
        diffs.push({id:prev[prop].id,prop,newValue:curr[prop]})
      }
    }
  }
  if(size(prev)<size(curr)){ 
    for(let prop in curr){
      if(Array.isArray(curr[prop])){//siblings
        curr[prop].forEach((sibling,i)=>{
          differ(prev[prop][i],sibling);
        })
      }else if(curr[prop]=="object"){
        for(let style in curr[prop]){
          cl(style)
          diff(prev[prop][style],curr[prop][style]);
        }
      }else{
        diff(prev,curr)//normal properties
      }
    }
  }
  return diffs;
}

let tree={};
let state={
  saved:{},
  save:function save(){
    this.saved=cloneNode(tree); // get global
  },
  update:function update(){
  	// use build tools from yeey
  }
}
function o(node){
	state.save();
	tree[node.id]=node;
}

o({
	id:"menu",
	class:"menu",
	styles:{
    	background:"black",
    	height:"100px"
  	},
  	eventListeners:[]
})
// a change
function style(node,styles){
  state.save(tree) // we need a copy/clone
  for(let style in styles){
    node.styles[style]=styles[style];
  }
  state.update();
  return node;
}

function select(nodeId){
  return tree[nodeId];
}
/*
  PSEUDO
	scan the whole obj for changes
  this is to be used on objects
  the top level function applies it from the outside in
  and registers every change made to the nodetree
  so i mean if we have an object
  like this one
  let obj={a:{b:1}}
  than we'd go from start main->a
  a could contain multiple objects, then you just run 
  another instance of diff, you will have to append it
  to a global(outer scope) variable to retain full diff.
  the updater reads in all changes (ordered to optimize!)
*/

/*
  for loop num of properties obj2
  compare object properties with obj1 properties
  conditions:
  1 prop is undefined
  2 prop value has been changed so we create desscriptor
   1 apply proper utility with descriptor
  3 prop is an object so we run another diff instance
   2 mutate layout properties
   3 save state
  4 Done..
*/
/*  process flow differ(output 
    descriptor->updater(apply correct fn)
*/

cl(differ(state.saved,tree))
function updater(diff){
  // mutate dom 23:49/1
  // string
  // array
  // object
  // number --> first stringify then? split? +=?
}
style(select("menu"),{
	background:"white",
})

cl(differ(state.saved,tree))